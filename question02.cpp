// c program to find the volume of the cone where the user input the height and radius of the cone

#include<stdio.h>
int main(){
	
	float radius,height,volume;
	printf("Enter radius : ");
	scanf("%f",&radius);
	printf("Enter height : ");
	scanf("%f",&height);
	
	volume=(22*radius*radius*height)/(3*7);
	printf("Volume of the cone: %f\n",volume);
	
	return 0;
	
}
