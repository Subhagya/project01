//c program to calculate the sum and average of any given 3 integers and print the output

#include<stdio.h>
int main(){
	//declare variables
	int num1,num2,num3;
	int sum, avg;
	
	//take input
	printf("Enter three numbers: ");
	scanf("%d %d %d", &num1,&num2,&num3);
	
	//calculate sum and average of numbers
	sum = num1 +num2 +num3;
	avg = sum / 3;
	
	printf("Entered numbers are: %d, %d, and %d\n",num1,num2,num3);
	
	//print the sum and avergae of 3 numbers
	printf("sum=%d\n",sum);
	printf("Average=%d\n",avg);
	return 0;	
}
