//c program to convert temparature from Celsius to Fahrenheit

#include<stdio.h>

int main(){
	
	 float celsius, fahrenheit;
	 
	 //input temparature in celsius
	 printf("Enter temparature in celsius: ");
	 scanf("%f",&celsius);
	 
	 //formula to convert celsius to fahrenheit
	 fahrenheit=(celsius * 9/5) + 32;
	 
	 printf("%.2f celsius= %.2f fahrenheit",celsius,fahrenheit);
	 
	 return 0;
}
