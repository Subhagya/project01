//c program demostrate bitwise operators using two integers

	#include<stdio.h>
	int main(){
	int A=10, B=15, C=0;
	
	C=A&B;
	printf("Bitwise AND operator- A&B= %d\n",C);
	
	C=A^B;
	printf("Bitwise XOR operator- A^B= %d\n",C);
	
	C=~A;
	printf("Bitwise One's Complement operator- ~A= %d\n",C);
	
	C=A<<3;
	printf("Bitwise left shift operator-A<<3= %d\n",C);
	
	C=B>>3;
	printf("Bitwise Right shift operator-B>>3= %d\n",C);
	
	return 0;
}
